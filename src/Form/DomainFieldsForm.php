<?php

namespace Drupal\domain_campos\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\domain_campos\Helpers;

/**
 * Class DomainFieldsForm.
 */
class DomainFieldsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'domain_campos.domainfields',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'domain_fields_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('domain_campos.domainfields');

        $get_domain = \Drupal::service('domain.negotiator');
        $domain = $get_domain->negotiateActiveHostname();

        $entity_type = 'node';

        /*Cargamos todos los typos de contenidos*/
        $bundles = \Drupal::entityManager()->getAllBundleInfo()['node'];

        $field_manager = \Drupal::service('entity_field.manager');

        $form['#tree'] = TRUE;
        $form['#entity_type'] = $entity_type;
        $form_state->set('entity_type', $entity_type);

        $form['#domain'] = $domain;
        $form_state->set('domain', $domain);
        foreach ($bundles as $key_bundle => $bundle) {
            $field_names = $field_manager->getFieldDefinitions('node', $key_bundle);

            $form[$entity_type][$key_bundle] = [
                '#type' => 'fieldset',
                '#title' => $this->t("@bundle", ['@bundle' => $key_bundle]),
            ];
            foreach ($field_names as $key_field => $field_name) {
                if ($field_name instanceof FieldConfig) {
                    $form[$entity_type][$key_bundle][$key_field] = [
                        '#type' => 'details',
                        '#title' => $this->t('@name', ['@name' => $key_field]),
                        '#open' => FALSE,
                    ];

                    $form[$entity_type][$key_bundle][$key_field]['required'] = [
                        '#type' => 'checkbox',
                        '#title' => $this->t('Force required'),
                        '#default_value' => Helpers::_domain_fields_instance_settings($entity_type, $key_bundle, $key_field, 'required', 0, $domain),
                    ];

                    $form[$entity_type][$key_bundle][$key_field]['edit'] = [
                        '#type' => 'checkbox',
                        '#title' => $this->t('Remove from forms'),
                        '#default_value' => Helpers::_domain_fields_instance_settings($entity_type, $key_bundle, $key_field, 'edit', 0, $domain),
                    ];
                    $form[$entity_type][$key_bundle][$key_field]['view'] = [
                        '#type' => 'checkbox',
                        '#title' => t('Remove from all displays'),
                        '#default_value' => Helpers::_domain_fields_instance_settings($entity_type, $key_bundle, $key_field, 'view', 0, $domain),
                    ];
                    $form[$entity_type][$key_bundle][$key_field]['label'] = [
                        '#type' => 'textfield',
                        '#title' => t('Label override'),
                        '#default_value' => Helpers::_domain_fields_instance_settings($entity_type, $key_bundle, $key_field, 'label', '', $domain),
                    ];
                    $form[$entity_type][$key_bundle][$key_field]['help'] = [
                        '#type' => 'textarea',
                        '#title' => t('Help override'),
                        '#default_value' => Helpers::_domain_fields_instance_settings($entity_type, $key_bundle, $key_field, 'help', '', $domain),
                    ];
                }
            }
        }

        if (!empty($form)) {
            $form['actions'] = ['#type' => 'actions'];
            $form['actions']['submit'] = [
                '#type' => 'submit',
                '#value' => t('Save'),
                '#submit' => ['domain_fields_settings_form_save_submit'],
            ];
            $form['actions']['reset'] = [
                '#type' => 'submit',
                '#value' => t('Reset'),
                '#submit' => ['domain_fields_settings_form_reset_submit'],
            ];
        }

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        parent::submitForm($form, $form_state);

        $domain_id = $form_state->get('domain');
        $entity_type = $form_state->get('entity_type');

        $settings = \Drupal::state()->get('domain_fields_' . $domain_id, []);
        $settings[$entity_type] = $form_state->getValues()[$entity_type];

        $this->config('domain_campos.domainfields')
            ->save();

        \Drupal::state()->set('domain_fields_' . $domain_id, $settings);
    }



}
